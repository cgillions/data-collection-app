package gillions.datacollection.communication;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Generic interface for de-coupled data handling.
 */

public interface IDataListener<T> {

    void onSuccess(T data);

    void onFailure(String error);
}
