package gillions.datacollection.mvp.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.presenter.impl.ActivitiesPresenter;
import gillions.datacollection.mvp.presenter.inter.IActivitiesPresenter;
import gillions.datacollection.mvp.view.impl.adapter.ActivityAdapter;
import gillions.datacollection.mvp.view.impl.adapter.ActivityListener;
import gillions.datacollection.mvp.view.impl.dialog.DeleteActivityDialog;
import gillions.datacollection.mvp.view.inter.IActivitiesView;
import io.realm.Realm;

/**
 * Created by Charlie on 07/02/2018.
 *
 * The activities view.
 */

public class ActivitiesFragment extends BaseFragment<IActivitiesView, IActivitiesPresenter> implements IActivitiesView, ActivityListener {

    private RecyclerView recyclerView;

    @Override
    public IActivitiesPresenter getPresenter() {
        return new ActivitiesPresenter(getBaseActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activities, container, false);

        // Initialise the recycler view.
        recyclerView = view.findViewById(R.id.recycler_activities);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        // Load the activities.
        loadActivities();

        // Attach a listener for adding new activities.
        view.findViewById(R.id.new_activity_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAddActivityClick();
            }
        });
        return view;
    }

    @Override
    public void onNewActivity() {
        int activityCount = loadActivities();
        recyclerView.smoothScrollToPosition(activityCount);
    }

    private int loadActivities() {
        // Query for the activities.
        Realm realm = Realm.getDefaultInstance();
        List<Activity> activities = realm.copyFromRealm(realm.where(Activity.class).findAll());
        realm.close();

        // Create an adapter for the recycler view.
        recyclerView.setAdapter(new ActivityAdapter(getContext(), activities, this));
        return activities.size();
    }

    @Override
    public void onActivityClick(Activity activity) {
        presenter.onActivityClick(activity);
    }

    @Override
    public void onActivityLongClick(final Activity activity) {
        new DeleteActivityDialog(getContext(), activity, new IDataListener<Void>() {
            @Override
            public void onSuccess(Void data) {
                loadActivities();
            }

            @Override
            public void onFailure(String error) {
                // Ignore.
            }
        }).show();
    }
}
