package gillions.datacollection.mvp.view.inter;

import android.support.v4.app.FragmentActivity;

import java.util.List;

import gillions.datacollection.mvp.model.ActivityPrediction;

/**
 * Created by Charlie on 03/03/2018.
 *
 * Interface for the testing view.
 */

public interface ITestView extends IView {

    FragmentActivity getActivity();

    void startState();

    void testState();

    void addPredictions(List<ActivityPrediction> activityPredictions);
}
