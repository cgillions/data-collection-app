package gillions.datacollection.mvp.view.inter;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Interface for the activities view.
 */

public interface IActivitiesView extends IView {


    void onNewActivity();
}
