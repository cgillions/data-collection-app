package gillions.datacollection.mvp.view.impl.animation;

import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;

public class BounceAnim extends ScaleAnimation {

    public BounceAnim() {
        super(0.95f, 1f,
                0.95f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        setInterpolator(new BounceInterpolator());
        setFillAfter(true);
        setDuration(500);
    }
}