package gillions.datacollection.mvp.view.impl.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import gillions.datacollection.R;
import gillions.datacollection.mvp.model.ActivityPrediction;

/**
 * Created by Charlie on 03/03/2018.
 *
 * Adapter to help display classified activities.
 */

public class PredictionsAdapter extends RecyclerView.Adapter<PredictionsAdapter.Holder> {

    private final ArrayList<ActivityPrediction> items;
    private final Calendar calendar = Calendar.getInstance();

    public PredictionsAdapter() {
        items = new ArrayList<>();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_activity,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        ActivityPrediction activity = items.get(position);

        calendar.setTimeInMillis(activity.getStartTime());
        String time = String.format(Locale.getDefault(), "%02d:%02d",
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

        String confidence = String.format(Locale.getDefault(),"Confidence: %.0f%%", activity.getConfidence() - 5);

        holder.name.setText(activity.getName());
        holder.duration.setText(time);
        holder.confidence.setText(confidence);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addPredictions(List<ActivityPrediction> activityPredictions) {
        int startIndex = items.size();
        items.addAll(activityPredictions);
        notifyItemRangeInserted(startIndex, activityPredictions.size());
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name, duration, confidence;

        Holder(View itemView) {
            super(itemView);
            name     = itemView.findViewById(R.id.title);
            duration = itemView.findViewById(R.id.completion);
            confidence = itemView.findViewById(R.id.confidence);
        }
    }
}
