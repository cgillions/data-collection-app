package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;

/**
 * Created by Charlie on 14/02/2018.
 *
 * Dialog to ask the user if they would like to delete the trial.
 */

public class VerifyTrialDialog extends Dialog {
    public VerifyTrialDialog(Context context, String filename, final IDataListener<Void> listener) {
        super(context);

        // Check the file exists.
        File trialFile = new File(filename);
        if (!trialFile.exists()) {
            return;
        }

        // Get the length of the file in bytes.
        long length = trialFile.length();
        if (length != 0) {
            // Convert to kb.
            length /= 1024L;
        }

        // Set the custom interface.
        setContentView(R.layout.dialog_verify_trial);
        setCancelable(false);

        // Set the size of the file on the UI.
        ((TextView) findViewById(R.id.file_size)).setText(
                String.format(Locale.getDefault(),"Size: %dkb", length)
        );

        // Attach a listener to the 'yes' button.
        findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // The user would like to keep the trial.
                // Return success.
                dismiss();
                listener.onSuccess(null);
            }
        });

        // Attach a listener to the 'no' button.
        findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // The user would like to delete the trial.
                // Return failure.
                dismiss();
                listener.onFailure(null);
            }
        });
    }
}
