package gillions.datacollection.mvp.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gillions.datacollection.R;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.presenter.impl.TrialPresenter;
import gillions.datacollection.mvp.presenter.inter.ITrialPresenter;
import gillions.datacollection.mvp.view.impl.animation.BounceAnim;
import gillions.datacollection.mvp.view.inter.ITrialView;

/**
 * Created by Charlie on 08/02/2018.
 *
 * The trial screen.
 */

public class TrialFragment extends BaseFragment<ITrialView, ITrialPresenter> implements ITrialView, View.OnClickListener, View.OnTouchListener {

    private Activity activity;
    private TextView buttonText, timerText;

    /**
     * Creates a new TrialFragment and passes it the activity.
     *
     * @param activity The activity to create a trial for
     * @return A new TrialFragment instance.
     */
    public static TrialFragment newInstance(Activity activity) {
        TrialFragment fragment = new TrialFragment();
        fragment.activity = activity;
        return fragment;
    }

    @Override
    public ITrialPresenter getPresenter() {
        return new TrialPresenter(activity, this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trial, container, false);

        // Set the necessary text.
        ((TextView) view.findViewById(R.id.title)).setText(activity.getTitle());
        ((TextView) view.findViewById(R.id.description)).setText(activity.getDescription());

        // Attach a click listener to the start button.
        View button = view.findViewById(R.id.btn_start);
        button.setOnTouchListener(this);
        button.setOnClickListener(this);

        buttonText = button.findViewById(R.id.start_text);
        timerText  = view.findViewById(R.id.timer_text);
        return view;
    }

    /**
     * Used for a simple animation.
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.startAnimation(new BounceAnim());
                return true;

            case MotionEvent.ACTION_UP:
                view.performClick();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onClick(View v) {
        presenter.onStartStop();
    }

    @Override
    public void startState() {
        buttonText.setText(R.string.start);
        timerText.setVisibility(View.GONE);
    }

    @Override
    public void trialState() {
        buttonText.setText(R.string.stop);
    }

    @Override
    public void setTimerText(String timeRemaining) {
        timerText.setText(timeRemaining);
        if (timerText.getVisibility() == View.GONE) {
            timerText.setVisibility(View.VISIBLE);
        }
    }
}
