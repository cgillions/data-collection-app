package gillions.datacollection.mvp.view.impl.activity;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.view.impl.fragment.BaseFragment;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Base class for all activities in the app.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private static final int STORAGE_REQUEST = 601;
    private IDataListener<Void> requestListener;

    /**
     * Method to get the resource ID for the fragment container.
     *
     * @return The resource ID for the activity's fragment container
     */
    protected abstract int getFragmentContainerResource();

    /**
     * Method to add a fragment to the fragment container of the activity.
     *
     * @param fragment       The fragment to add.
     * @param addToBackstack True if the transaction should be added to the back stack.
     */
    public void goToFragment(BaseFragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackstack) {
            transaction.addToBackStack(null);
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        }

        transaction.replace(getFragmentContainerResource(), fragment).commit();
    }

    /**
     * Function to request WRITE_EXTERNAL_STORAGE permission.
     *
     * @param requestListener The listener to pass the result on to
     */
    public void requestWriteStoragePermission(IDataListener<Void> requestListener) {
        // Check if we've already been granted this permission.
        if (ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED) {
            requestListener.onSuccess(null);
        } else {
            // Request the permission.
            this.requestListener = requestListener;
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        super.onRequestPermissionsResult(requestCode, permissions, results);

        // Check it's our request.
        if (requestListener != null && requestCode == STORAGE_REQUEST) {
            if (results.length > 0 && results[0] == PERMISSION_GRANTED) {
                requestListener.onSuccess(null);
            } else {
                requestListener.onFailure(null);
            }
        }
    }
}
