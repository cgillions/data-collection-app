package gillions.datacollection.mvp.view.inter;

import android.content.Context;

/**
 * Created by Charlie on 08/02/2018.
 *
 * Interface for the trial view.
 */

public interface ITrialView extends IView {

    /**
     * Sets the view to the starting state.
     */
    void startState();

    /**
     * Sets the view to a trial state.
     */
    void trialState();

    /**
     * Get the current context from the view.
     *
     * @return Valid context
     */
    Context getContext();

    /**
     * Update the time remaining label.
     *
     * @param timeRemaining The remaining time of the trial
     */
    void setTimerText(String timeRemaining);
}
