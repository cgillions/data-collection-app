package gillions.datacollection.mvp.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import gillions.datacollection.mvp.presenter.inter.IPresenter;
import gillions.datacollection.mvp.view.inter.IView;
import gillions.datacollection.mvp.view.impl.activity.BaseActivity;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Base class for all fragments in the app.
 */

public abstract class BaseFragment<V extends IView, P extends IPresenter<V>> extends Fragment implements IView {

    protected P presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = getPresenter();
    }

    @Override
    public void onDestroy() {
        presenter = null;
        super.onDestroy();
    }

    protected abstract P getPresenter();

    /**
     * Function to get the BaseActivity class.
     */
    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }
}
