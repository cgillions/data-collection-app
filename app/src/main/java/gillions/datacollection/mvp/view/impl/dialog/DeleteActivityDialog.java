package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.controller.ActivityController;
import gillions.datacollection.mvp.model.Activity;
import io.realm.Realm;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Dialog to help add an activity to the application.
 */

public class DeleteActivityDialog extends Dialog {

    /**
     * Constructs an AddActivityDialog.
     *
     * @param context Context to construct dialog.
     * @param activity The activity to delete
     */
    public DeleteActivityDialog(final Context context, final Activity activity, final IDataListener<Void> listener) {
        super(context);

        // Set the custom interface.
        setContentView(R.layout.dialog_activity_delete);

        // Attach a listener to the 'yes' button.
        findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.where(Activity.class)
                        .equalTo("id", activity.getId())
                        .findFirst()
                        .deleteFromRealm();
                realm.commitTransaction();
                realm.close();
                dismiss();
                listener.onSuccess(null);

            }
        });

        // Attach a listener to the 'cancel' button.
        findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
