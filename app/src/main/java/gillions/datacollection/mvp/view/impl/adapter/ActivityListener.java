package gillions.datacollection.mvp.view.impl.adapter;

import gillions.datacollection.mvp.model.Activity;

/**
 * Created by Charlie on 04/05/2018.
 */
public interface ActivityListener {
    void onActivityClick(Activity activity);

    void onActivityLongClick(Activity activity);
}
