package gillions.datacollection.mvp.view.impl.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import gillions.datacollection.R;
import gillions.datacollection.controller.ActivityController;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.model.Preferences;
import gillions.datacollection.mvp.view.impl.animation.BounceAnim;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityHolder> implements View.OnTouchListener {

    private final int userId;
    private final Context context;
    private final List<Activity> activities;
    private final ActivityListener listener;

    public ActivityAdapter(Context context, List<Activity> activities, ActivityListener listener) {
        userId = Preferences.getUserId(context);
        this.activities = activities;
        this.listener   = listener;
        this.context    = context;
    }

    @NonNull
    @Override
    public ActivityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate the layout.
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_activity, parent, false);

        return new ActivityHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityHolder holder, int position) {
        // Get the activity.
        final Activity activity = activities.get(position);

        // Set the title.
        holder.title.setText(activity.getTitle());

        // Find out how many of these activities the user has completed.
        int noCompleted = ActivityController.getCompletedActivityCount(userId, activity.getTitle());

        // Set the completion level.
        holder.completion.setText(String.format(Locale.getDefault(), "%d/%d", noCompleted, activity.getMaxTrials()));

        // Attach a listener to the view in order to animate it.
        View parent = (View) holder.title.getParent();
        parent.setOnTouchListener(this);

        // Attach a click listener in order to pass the activity to the listener.
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onActivityClick(activity);
            }
        });

        parent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listener.onActivityLongClick(activity);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    /**
     * Used for a simple animation.
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.startAnimation(new BounceAnim());
                return false;

            case MotionEvent.ACTION_UP:
                view.performClick();
                return true;

            default:
                return false;
        }
    }

    class ActivityHolder extends RecyclerView.ViewHolder {

        private final TextView title, completion;

        private ActivityHolder(View itemView) {
            super(itemView);
            title      = itemView.findViewById(R.id.title);
            completion = itemView.findViewById(R.id.completion);
        }
    }
}
