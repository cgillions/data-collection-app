package gillions.datacollection.mvp.view.impl.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.view.impl.fragment.WelcomeFragment;

import static gillions.datacollection.controller.RealmController.initDatabase;

/**
 * Created by Charlie on 07/02/2018.
 *
 * The main activity for the app.
 */

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialise the database.
        initDatabase(this);

        // Request permission to write to Documents folder.
        requestWriteStoragePermission(new IDataListener<Void>() {
            @Override
            public void onSuccess(Void data) {
                // All good, ignore.
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(MainActivity.this,
                        "We need this permission to store trial data!",
                        Toast.LENGTH_SHORT).show();

                // Recursively call until we get permission...
                requestWriteStoragePermission(this);
            }
        });

        // Go to the welcome screen.
        goToFragment(new WelcomeFragment(), false);
    }

    /**
     * Method to get the resource ID for the fragment container.
     *
     * @return The resource ID for the activity's fragment container
     */
    @Override
    public int getFragmentContainerResource() {
        return R.id.container;
    }
}
