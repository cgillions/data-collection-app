package gillions.datacollection.mvp.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import gillions.datacollection.R;
import gillions.datacollection.mvp.model.ActivityPrediction;
import gillions.datacollection.mvp.model.Model;
import gillions.datacollection.mvp.presenter.impl.TestPresenter;
import gillions.datacollection.mvp.presenter.inter.ITestPresenter;
import gillions.datacollection.mvp.view.impl.adapter.PredictionsAdapter;
import gillions.datacollection.mvp.view.impl.animation.BounceAnim;
import gillions.datacollection.mvp.view.inter.ITestView;

/**
 * Created by Charlie on 03/03/2018.
 *
 * A screen to show live classification of data.
 */

public class TestFragment extends BaseFragment<ITestView, ITestPresenter> implements ITestView, View.OnTouchListener, View.OnClickListener {

    private TextView buttonText;
    private RecyclerView recyclerView;
    private PredictionsAdapter predictionsAdapter;

    @Override
    protected ITestPresenter getPresenter() {
        return new TestPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        // Attach a click listener to the start button.
        View button = view.findViewById(R.id.btn_start);
        buttonText = button.findViewById(R.id.start_text);
        button.setOnTouchListener(this);
        button.setOnClickListener(this);

        // Set up the recycler view.
        recyclerView = view.findViewById(R.id.recycler_predictions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        predictionsAdapter = new PredictionsAdapter();
        recyclerView.setAdapter(predictionsAdapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getModel();
    }

    /**
     * Used for a simple animation.
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.startAnimation(new BounceAnim());
                return true;

            case MotionEvent.ACTION_UP:
                view.performClick();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onClick(View v) {
        presenter.onStartStop();
    }

    @Override
    public void startState() {
        buttonText.setText(R.string.start);
    }

    @Override
    public void testState() {
        buttonText.setText(R.string.stop);
    }

    @Override
    public void addPredictions(List<ActivityPrediction> activityPredictions) {
        predictionsAdapter.addPredictions(activityPredictions);
        recyclerView.smoothScrollToPosition(predictionsAdapter.getItemCount());
    }
}
