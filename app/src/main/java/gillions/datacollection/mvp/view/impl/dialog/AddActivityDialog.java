package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.controller.ActivityController;
import gillions.datacollection.mvp.model.Preferences;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Dialog to help add an activity to the application.
 */

public class AddActivityDialog extends Dialog {

    /**
     * Constructs an AddActivityDialog.
     *
     * @param context Context to construct dialog.
     * @param listener This class will be notified 'onSuccess' if add is successful
     *                 and 'onFailure' if the user presses cancel.
     */
    public AddActivityDialog(final Context context, final IDataListener<Void> listener) {
        super(context);

        // Set the custom interface.
        setContentView(R.layout.dialog_activity_name);

        // Find the necessary views.
        final EditText editText = findViewById(R.id.edit_activity_name);

        // Attach a listener to the 'add' button.
        findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the user's input.
                String text = editText.getText().toString();

                if (text.length() < 3) {
                    editText.setError("Enter a valid activity name");
                    return;
                }

                if (ActivityController.isNewActivity(text)) {
                    ActivityController.addActivity(text);
                    listener.onSuccess(null);
                    dismiss();
                } else {
                    editText.setError("This activity already exists!");
                }
            }
        });

        // Attach a listener to the 'cancel' button.
        findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
