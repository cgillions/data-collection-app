package gillions.datacollection.mvp.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import gillions.datacollection.R;
import gillions.datacollection.mvp.presenter.impl.WelcomePresenter;
import gillions.datacollection.mvp.presenter.inter.IWelcomePresenter;
import gillions.datacollection.mvp.view.impl.animation.BounceAnim;
import gillions.datacollection.mvp.view.inter.IWelcomeView;

/**
 * Created by Charlie on 07/02/2018.
 *
 * The first content to be shown when opening the app.
 */

public class WelcomeFragment extends BaseFragment<IWelcomeView, IWelcomePresenter> implements IWelcomeView, View.OnTouchListener, View.OnClickListener {

    @Override
    protected IWelcomePresenter getPresenter() {
        return new WelcomePresenter(getBaseActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);

        // Find the layouts.
        View btnActivity  = view.findViewById(R.id.btn_activity);
        View btnConnect   = view.findViewById(R.id.btn_connect);
        View btnTest      = view.findViewById(R.id.btn_test);

        // Attach listeners.
        btnActivity.setOnTouchListener(this);
        btnActivity.setOnClickListener(this);
        btnConnect.setOnTouchListener(this);
        btnConnect.setOnClickListener(this);
        btnTest.setOnTouchListener(this);
        btnTest.setOnClickListener(this);
        return view;
    }

    /**
     * Used for a simple animation.
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.startAnimation(new BounceAnim());
                return true;

            case MotionEvent.ACTION_UP:
                view.performClick();
                return true;

            default:
                return false;
        }
    }

    /**
     * Handle button clicks.
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_activity:
                presenter.onSeeActivitiesClick();
                break;

            case R.id.btn_connect:
                presenter.onConnectBandClick();
                break;

            case R.id.btn_test:
                presenter.onTestClick();
        }
    }
}