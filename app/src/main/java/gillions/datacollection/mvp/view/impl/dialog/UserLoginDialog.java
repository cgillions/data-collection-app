package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.controller.ActivityController;
import gillions.datacollection.mvp.model.Preferences;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Dialog to help uniquely identify the user.
 */

public class UserLoginDialog extends Dialog {

    /**
     * Constructs a login dialog.
     *
     * @param context Context to construct dialog.
     * @param listener This class will be notified 'onSuccess' if login is successful
     *                         and 'onFailure' if the user presses cancel.
     */
    public UserLoginDialog(final Context context, final IDataListener<Void> listener) {
        super(context);

        // Set the custom interface.
        setContentView(R.layout.dialog_user_id);

        // Find the necessary views.
        final EditText editText = findViewById(R.id.edit_user_id);

        // Attach a listener to the 'confirm' button.
        findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the user's input.
                String text = editText.getText().toString();

                try {
                    // Verify a valid integer has been entered.
                    final int id = Integer.parseInt(text);

                    // Verify a valid ID has been entered.
                    if (id < 0) {
                        // Inform the user of the error.
                        editText.setError(context.getString(R.string.invalid_id));
                    } else {
                        // Has the user signed their consent form?
                        if (ActivityController.isNewUser(id)) {
                            new ConsentDialog(context, new IDataListener<Void>() {
                                @Override
                                public void onSuccess(Void data) {
                                    // The user has signed their consent form.  Store the user ID.
                                    Preferences.setUserId(getContext(), id);
                                    listener.onSuccess(data);
                                }

                                @Override
                                public void onFailure(String error) {
                                    // The user hasn't signed their consent form. Remain.
                                    listener.onFailure(error);
                                }
                            }).show();
                        } else {
                            // It's not a new user. Let them proceed and store their user ID.
                            Preferences.setUserId(getContext(), id);
                            listener.onSuccess(null);
                        }
                    }

                } catch (NumberFormatException ex) {
                    // Inform the user of the error.
                    editText.setError(context.getString(R.string.invalid_id));
                }
            }
        });

        // Attach a listener to the 'cancel' button.
        findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFailure(null);
            }
        });
    }
}
