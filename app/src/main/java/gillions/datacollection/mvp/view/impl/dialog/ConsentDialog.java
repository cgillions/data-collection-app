package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;

/**
 * Created by Charlie on 08/02/2018.
 *
 * Class to handle checking if the user has signed their consent form.
 */

public class ConsentDialog extends Dialog {

    public ConsentDialog(@NonNull final Context context, final IDataListener<Void> listener) {
        super(context);
        setContentView(R.layout.dialog_consent);

        // Handle consent form signed.
        findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSuccess(null);
                dismiss();
            }
        });

        // Handle no consent form signed.
        findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Please see the app owner and request a consent form.", Toast.LENGTH_LONG).show();
                listener.onFailure(null);
                dismiss();
            }
        });

        // If the user dismisses the dialog, we should not proceed.
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                listener.onFailure(null);
            }
        });
    }
}