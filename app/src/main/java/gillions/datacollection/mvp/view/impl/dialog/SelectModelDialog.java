package gillions.datacollection.mvp.view.impl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.List;
import java.util.Locale;

import gillions.datacollection.R;
import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.model.Model;
import gillions.datacollection.mvp.view.impl.adapter.ModelAdapter;

/**
 * Created by Charlie on 14/02/2018.
 *
 * Dialog to ask the user if they would like to delete the trial.
 */

public class SelectModelDialog extends Dialog {
    public SelectModelDialog(Context context, List<Model> models, final IDataListener<Model> listener) {
        super(context);

        // Set the custom interface.
        setContentView(R.layout.dialog_select_model);
        setCancelable(false);

        RecyclerView recyclerView = findViewById(R.id.model_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        ModelAdapter adapter = new ModelAdapter(models, new IDataListener<Model>(){
            @Override
            public void onSuccess(Model data) {
                listener.onSuccess(data);
                dismiss();
            }

            @Override
            public void onFailure(String error) {
                listener.onFailure(error);
                dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        show();
    }
}
