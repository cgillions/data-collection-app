package gillions.datacollection.mvp.presenter.inter;

import gillions.datacollection.mvp.view.inter.ITrialView;

/**
 * Created by Charlie on 08/02/2018.
 *
 * Interface for the trial screen presenter.
 */

public interface ITrialPresenter extends IPresenter<ITrialView> {

    /**
     * Function to handle logic when the start or stop button is pressed.
     */
    void onStartStop();
}
