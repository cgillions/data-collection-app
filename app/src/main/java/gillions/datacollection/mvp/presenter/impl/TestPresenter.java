package gillions.datacollection.mvp.presenter.impl;

import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.controller.BandController;
import gillions.datacollection.mvp.model.ActivityPrediction;
import gillions.datacollection.mvp.model.Model;
import gillions.datacollection.mvp.presenter.inter.ITestPresenter;
import gillions.datacollection.mvp.view.impl.dialog.SelectModelDialog;
import gillions.datacollection.mvp.view.inter.ITestView;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;

/**
 * Created by Charlie on 03/03/2018.
 *
 * Presenter for the test view.
 */

public class TestPresenter implements ITestPresenter, BandController.IBandEventListener {

    private final ITestView view;
    private final RequestQueue requestQueue;
    private String modelName;
    private boolean started;

    public TestPresenter(ITestView view) {
        this.view = view;
        requestQueue = Volley.newRequestQueue(view.getActivity());
    }

    @Override
    public void getModel() {
        // Make the network request on a background thread.
        new GetModelTask(this).execute();
    }

    private void onNewModels(List<Model> models) {
        // Select the model.
        new SelectModelDialog(view.getActivity(), models, new IDataListener<Model>() {
            @Override
            public void onSuccess(Model data) {
                modelName = data.getName();
            }

            @Override
            public void onFailure(String error) {
                // Ignore.
            }
        });
    }

    /**
     * Function to handle logic when the start or stop button is pressed.
     */
    @Override
    public void onStartStop() {
        if (started) {
            // Handle stopping recording accelerometer data.
            handleStop(true);
        } else {
            handleStart();
        }
    }

    /**
     * Internal method to handle the beginning of a trial.
     */
    private void handleStart() {
        started = true;
        view.testState();

        // Subscribe to events from the microsoft band.
        BandController controller = BandController.getInstance(view.getActivity());
        controller.subscribe(this);
        controller.startListening();
    }

    /**
     * Internal method to handle the end of a trial.
     */
    private void handleStop(boolean stopListening) {
        started = false;
        view.startState();
        acceleration_data = "";

        // Unsubscribe from events from the microsoft band.
        BandController controller = BandController.getInstance(view.getActivity());
        controller.unsubscribe(this);

        if (stopListening) {
            controller.stopListening();
        }
    }

    @Override
    public void onNewPredictions(List<ActivityPrediction> activityPredictions) {
        if (view != null && view.getActivity() != null) {
            view.addPredictions(activityPredictions);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Band event callbacks.
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private String acceleration_data;
    private long startTime;

    @Override
    public void onConnected() {
        acceleration_data = "";
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onDisconnected() {
        handleStop(false);
    }

    @Override
    public void onError(String error) {
        handleStop(true);
    }

    @Override
    public void onData(String data) {
        acceleration_data += data;

        if (System.currentTimeMillis() > (startTime + 10000)) {
            new ClassifierTask(this).execute(acceleration_data);
            startTime = System.currentTimeMillis();
            acceleration_data = "";
        }
    }

    @Override
    public void onTrialComplete(String filename) {

    }

    private static class GetModelTask extends AsyncTask<Void, Void, Void> {

        private final TestPresenter presenter;

        GetModelTask(TestPresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new StringRequest(GET, "https://ml-training.herokuapp.com/models", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jsonResponse;
                    List<Model> models = new ArrayList<>();
                    try {
                        jsonResponse = new JSONObject(response);

                        // Get the models.
                        JSONArray jsonModels = jsonResponse.getJSONArray("models");
                        for (int i = 0; i < jsonModels.length(); i++) {
                            JSONObject model = jsonModels.getJSONObject(i);

                            // Create objects from the json.
                            models.add(new Model(model.getString("name"),
                                                 model.getString("description")));
                        }

                        presenter.onNewModels(models);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }).setRetryPolicy(new DefaultRetryPolicy(10000, 0, 1));
            presenter.requestQueue.add(request);
            return null;
        }
    }

    private static class ClassifierTask extends AsyncTask<String, Void, Void> {

        private final TestPresenter presenter;

        ClassifierTask(TestPresenter testPresenter) {
            presenter = testPresenter;
        }

        @Override
        protected Void doInBackground(final String... data) {
            // Create the data for the POST request.
            final Map<String, String> postData = new HashMap<String, String>() {{
                put("acceleration", data[0]);
                put("model_name", presenter.modelName);
            }};

            // Create a Volley request to the Activity Classification Service.
            Request request = new StringRequest(POST, "https://ml-training.herokuapp.com/classify",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            List<ActivityPrediction> activityPredictions = new ArrayList<>();

                            try {
                                JSONObject jsonResponse = new JSONObject(response);

                                // Get the classified activities.
                                JSONArray jsonActivities = jsonResponse.getJSONArray("predictions");
                                for (int i = 0; i < jsonActivities.length(); i++) {
                                    JSONObject activity = jsonActivities.getJSONObject(i);

                                    // Create objects from the json.
                                    activityPredictions.add(new ActivityPrediction(
                                            activity.getString("name"),
                                            activity.getLong("start_time"),
                                            activity.getLong("end_time"),
                                            activity.getDouble("confidence")
                                    ));
                                }
                                presenter.onNewPredictions(activityPredictions);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return postData;
                }
            }.setRetryPolicy(new DefaultRetryPolicy(10000, 0, 1));
            presenter.requestQueue.add(request);

            return null;
        }
    }
}
