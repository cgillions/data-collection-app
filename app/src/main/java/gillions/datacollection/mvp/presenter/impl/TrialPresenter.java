package gillions.datacollection.mvp.presenter.impl;

import android.os.Handler;
import android.widget.Toast;

import java.util.Locale;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.controller.ActivityController;
import gillions.datacollection.controller.BandController;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.model.Preferences;
import gillions.datacollection.mvp.presenter.inter.ITrialPresenter;
import gillions.datacollection.mvp.view.impl.dialog.VerifyTrialDialog;
import gillions.datacollection.mvp.view.inter.ITrialView;

/**
 * Created by Charlie on 08/02/2018.
 *
 * Presenter for the trial view.
 */

public class TrialPresenter implements ITrialPresenter, IDataListener<String> {

    // Trials last for one minute.
    private static final int TRIAL_DURATION_SECS = 60;
    private int timeRemaining = TRIAL_DURATION_SECS;
    private boolean started = false;

    private final Activity   activity;
    private final ITrialView view;

    private final int userId;

    // Create a handler to schedule a task.
    private Handler handler = new Handler();

    // Runnable to update the UI each second of the trial.
    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {
            // Format the time remaining to display seconds.
            view.setTimerText(String.format(Locale.getDefault(), "%ds", timeRemaining));
            timeRemaining -= 1;

            // Loop until the trial ends.
            if (timeRemaining > 0) {
                handler.postDelayed(updateRunnable, 1000);
            } else {
                // Reset the time.
                timeRemaining = TRIAL_DURATION_SECS;
            }
        }
    };

    public TrialPresenter(Activity activity, ITrialView view) {
        this.activity = activity;
        this.view     = view;

        // Get the user ID.
        userId = Preferences.getUserId(view.getContext());
    }

    /**
     * Function to handle logic when the start or stop button is pressed.
     */
    @Override
    public void onStartStop() {
        if (started) {
            // Handle stopping recording accelerometer data.
            handleStop(true);
        } else {
            handleStart();
        }
    }

    /**
     * Internal method to handle the beginning of a trial.
     */
    private void handleStart() {
        started = true;
        view.trialState();
        timeRemaining = TRIAL_DURATION_SECS;
        handler.post(updateRunnable);

        // Start recording the activity.
        ActivityController.recordActivity(view.getContext(), this,
                userId, activity.getTitle(), TRIAL_DURATION_SECS * 1000);
    }

    /**
     * Internal method to handle the end of a trial.
     */
    private void handleStop(boolean stopRecording) {
        started = false;
        handler.removeCallbacks(updateRunnable);
        view.startState();

        if (stopRecording) {
            BandController.getInstance((android.app.Activity) view.getContext()).stopListening();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Record activity callbacks
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Called when a trial ends successfully.
     *
     * @param data Null
     */
    @Override
    public void onSuccess(String data) {
        // Check if the view associated with the trial exists.
        if (view == null || view.getContext() == null) {

            // If not, delete the file as there could have been an error.
            ActivityController.deleteLastTrial(userId, activity.getTitle());
            return;
        }

        // Update the UI accordingly.
        // No need to stop recording, that's already happened.
        handleStop(false);

        // Verify that we want to keep this trial's data.
        new VerifyTrialDialog(view.getContext(), data, new IDataListener<Void>() {
            @Override
            public void onSuccess(Void data) {
                // We do want to keep the trial. No action is needed.
            }

            @Override
            public void onFailure(String error) {
                // We don't want to keep the trial. Delete the data file.
                if (ActivityController.deleteLastTrial(Preferences.getUserId(view.getContext()),
                        activity.getTitle())) {
                    Toast.makeText(view.getContext(), "Deleted trial data successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(view.getContext(), "Unable to delete trial data", Toast.LENGTH_SHORT).show();
                }
            }
        }).show();
    }

    /**
     * Called when a trial ends unsuccessfully.
     *
     * @param error The error
     */
    @Override
    public void onFailure(String error) {
        // Check if the view associated with the trial exists.
        if (view == null || view.getContext() == null) {

            // If not, delete the file as there could have been an error.
            ActivityController.deleteLastTrial(userId, activity.getTitle());
            return;
        }

        // Update the UI accordingly.
        // No need to stop recording, that's already happened.
        handleStop(false);
        Toast.makeText(view.getContext(), error, Toast.LENGTH_SHORT).show();
    }
}