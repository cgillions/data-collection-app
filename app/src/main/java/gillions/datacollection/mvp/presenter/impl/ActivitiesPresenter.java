package gillions.datacollection.mvp.presenter.impl;

import android.app.Dialog;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.presenter.inter.IActivitiesPresenter;
import gillions.datacollection.mvp.view.impl.activity.BaseActivity;
import gillions.datacollection.mvp.view.impl.dialog.AddActivityDialog;
import gillions.datacollection.mvp.view.impl.fragment.TrialFragment;
import gillions.datacollection.mvp.view.inter.IActivitiesView;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Presenter for the activities view.
 */

public class ActivitiesPresenter implements IActivitiesPresenter {

    private final BaseActivity activity;
    private final IActivitiesView view;

    public ActivitiesPresenter(BaseActivity activity, IActivitiesView view) {
        this.activity = activity;
        this.view = view;
    }

    @Override
    public void onAddActivityClick() {
        Dialog dialog = new AddActivityDialog(activity, new IDataListener<Void>() {
            @Override
            public void onSuccess(Void data) {
                view.onNewActivity();
            }

            @Override
            public void onFailure(String error) {
                // Ignore.
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityClick(Activity activity) {
        this.activity.goToFragment(TrialFragment.newInstance(activity), true);
    }
}
