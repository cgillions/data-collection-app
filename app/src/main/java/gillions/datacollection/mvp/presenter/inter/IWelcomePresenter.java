package gillions.datacollection.mvp.presenter.inter;

import gillions.datacollection.mvp.view.inter.IWelcomeView;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Interface for the WelcomePresenter.
 */

public interface IWelcomePresenter extends IPresenter<IWelcomeView> {

    /**
     * Function to handle logic before going to the activities screen.
     */
    void onSeeActivitiesClick();

    /**
     * Function to connect to the Microsoft Band 2.
     */
    void onConnectBandClick();

    /**
     * Function to allow the user to test the classification system.
     */
    void onTestClick();
}
