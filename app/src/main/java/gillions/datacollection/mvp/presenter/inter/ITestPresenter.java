package gillions.datacollection.mvp.presenter.inter;

import java.util.List;

import gillions.datacollection.mvp.model.ActivityPrediction;
import gillions.datacollection.mvp.view.inter.ITestView;

/**
 * Created by Charlie on 03/03/2018.
 *
 * Presenter for the test view.
 */

public interface ITestPresenter extends IPresenter<ITestView> {

    void getModel();

    void onStartStop();

    void onNewPredictions(List<ActivityPrediction> predictions);
}
