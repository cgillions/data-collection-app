package gillions.datacollection.mvp.presenter.inter;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.view.inter.IActivitiesView;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Interface for the activities presenter.
 */

public interface IActivitiesPresenter extends IPresenter<IActivitiesView> {

    void onAddActivityClick();

    void onActivityClick(Activity activity);
}
