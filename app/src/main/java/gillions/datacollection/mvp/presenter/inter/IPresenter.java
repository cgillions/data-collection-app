package gillions.datacollection.mvp.presenter.inter;

import gillions.datacollection.mvp.view.inter.IView;

/**
 * Created by Charlie on 07/02/2018.
 *
 * This is a base interface for all presenters under the MVP architecture.
 */

public interface IPresenter<V extends IView> {
}
