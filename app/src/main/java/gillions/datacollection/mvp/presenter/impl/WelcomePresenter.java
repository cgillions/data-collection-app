package gillions.datacollection.mvp.presenter.impl;

import android.app.Dialog;
import android.content.Intent;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.presenter.inter.IWelcomePresenter;
import gillions.datacollection.mvp.view.impl.activity.BaseActivity;
import gillions.datacollection.mvp.view.impl.dialog.UserLoginDialog;
import gillions.datacollection.mvp.view.impl.fragment.ActivitiesFragment;
import gillions.datacollection.mvp.view.impl.fragment.TestFragment;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Class to handle logic on the Welcome screen.
 */

public class WelcomePresenter implements IWelcomePresenter {

    private BaseActivity activity;
    private final LoginHandler loginHandler;

    public WelcomePresenter(BaseActivity activity) {
        this.activity = activity;
        loginHandler  = new LoginHandler(activity);
    }

    @Override
    public void onSeeActivitiesClick() {
        loginHandler.attemptLogin();
    }

    @Override
    public void onConnectBandClick() {
        Intent settingsIntent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        activity.startActivity(settingsIntent);
    }

    /**
     * Class to handle logging a participant in, and ensuring their consent form is signed.
     */
    private class LoginHandler implements IDataListener<Void> {

        private final BaseActivity activity;
        private final Dialog loginDialog;

        private LoginHandler(BaseActivity activity) {
            loginDialog = new UserLoginDialog(activity, this);
            this.activity = activity;
        }

        private void attemptLogin() {
            loginDialog.show();
        }

        @Override
        public void onSuccess(Void data) {
            loginDialog.dismiss();

            // Go to the activities fragment.
            activity.goToFragment(new ActivitiesFragment(), true);
        }

        @Override
        public void onFailure(String error) {
            loginDialog.dismiss();
        }
    }

    @Override
    public void onTestClick() {
        activity.goToFragment(new TestFragment(), true);
    }
}