package gillions.datacollection.mvp.model;

/**
 * Created by Charlie on 19/05/2018.
 */

public class Model {

    private String name, description;

    public Model(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
