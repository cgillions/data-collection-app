package gillions.datacollection.mvp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Data class for an Activity.
 */

public class Activity extends RealmObject {

    @PrimaryKey
    private int id;
    private int maxTrials;
    private String title, description;

    public Activity() {}

    public Activity(int id, String title, int maxTrials, String description) {
        this.id = id;
        this.title = title;
        this.maxTrials = maxTrials;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public int getMaxTrials() {
        return maxTrials;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}