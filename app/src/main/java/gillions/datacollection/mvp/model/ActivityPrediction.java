package gillions.datacollection.mvp.model;

/**
 * Created by Charlie on 03/03/2018.
 */

public class ActivityPrediction {

    private String name;
    private long startTime, endTime;
    private double confidence;

    public ActivityPrediction(String name, long startTime, long endTime, double confidence) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.confidence = confidence;
    }

    public String getName() {
        return name;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public double getConfidence() {
        return confidence;
    }
}
