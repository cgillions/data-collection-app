package gillions.datacollection.mvp.model;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static final String
                        PREFERENCES_FILE = "preferences",
                        DATABASE_VERSION = "db_version",
                        USER_ID          = "user_id";

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    public static int getUserId(Context context) {
        return getPreferences(context).getInt(USER_ID, -1);
    }

    public static void setUserId(Context context, int id) {
        getPreferences(context).edit().putInt(USER_ID, id).apply();
    }

    public static long getDatabaseVersion(Context context) {
        return getPreferences(context).getLong(DATABASE_VERSION, 0);
    }

    public static void setDatabaseVersion(Context context, long version) {
        getPreferences(context).edit().putLong(DATABASE_VERSION, version).apply();
    }
}
