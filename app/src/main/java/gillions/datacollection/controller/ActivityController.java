package gillions.datacollection.controller;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import gillions.datacollection.communication.IDataListener;
import gillions.datacollection.mvp.model.Activity;
import io.realm.Realm;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Class to handle activity-related functionality.
 */

public class ActivityController {

    private static TrialRecorder trialRecorder;

    private static int MAX_TRIALS = 5;
    private static String DEFAULT_DESCRIPTION = "Follow the instructions provided by the data collector.";

    /**
     * Function to get the activities to be completed by each user.
     *
     * @return A list of activities.
     */
    public static List<Activity> getActivities() {
        List<Activity> activities = new ArrayList<>();
        activities.add(new Activity(0, "Sitting",  MAX_TRIALS, "You will need to sit for one minute. Don't worry about fidgeting.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop. \n"));

        activities.add(new Activity(1, "Standing",  MAX_TRIALS, "You will need to stand for one minute. Don't worry about fidgeting.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop. \n"));

        activities.add(new Activity(2, "On Phone (sit)",  MAX_TRIALS, "You will need to sit and be on your phone for one minute.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop. \n"));

        activities.add(new Activity(3, "On Phone (stand)",  MAX_TRIALS, "You will need to stand and be on your phone for one minute.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop. \n"));

        activities.add(new Activity(4, "Writing",  MAX_TRIALS, "Hand-write the given text for one minute. If you reach the end of the text, just start again from the beginning.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop writing. \n"));

        activities.add(new Activity(5, "Typing",  MAX_TRIALS, "Type the given text for one minute. If you reach the end of the text, just start again from the beginning.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop typing. \n"));

        activities.add(new Activity(6, "Walking",  MAX_TRIALS, "You will need to walk as naturally as possible for one minute.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop walking. \n"));

        activities.add(new Activity(7, "Jogging",  MAX_TRIALS, "You will need to jog as naturally as possible for one minute.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop jogging. \n"));

        activities.add(new Activity(8, "Cycling",  MAX_TRIALS, "You will need to cycle for one minute.\n\n" +
                "When ready, pass the phone to the data collector who will start the recording shortly after.\n\n" +
                "You will be notified when you can stop cycling. \n"));

        return activities;
    }

    /**
     * Returns all of the user directories.
     *
     * @return A list of user directories.
     */
    private static List<File> getUserDirectories() {
        List<File> userFiles = new ArrayList<>();
        File appDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "DataCollection");
        if (appDirectory.list() != null) {
            for (String filename : appDirectory.list()) {
                if (filename.length() > 5 && filename.substring(0, 5).equals("user_")) {
                    userFiles.add(new File(appDirectory, filename));
                }
            }
        }
        return userFiles;
    }

    /**
     * Gets the number of completed activity trials for a particular user.
     *
     * @param userId The unique ID of the participant
     * @param activityName The name of the activity
     * @return Number of completed activities.
     */
    public static int getCompletedActivityCount(int userId, String activityName) {
        // Open the application files.
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "DataCollection");

        // Open the user-specific directory.
        File userDirectory = new File(directory, String.format(Locale.getDefault(), "user_%d", userId));
        if (!userDirectory.exists()) {
            return 0;
        }

        // Open the activity-specific directory.
        File activityDirectory = new File(userDirectory, activityName);
        if (!activityDirectory.exists()) {
            return 0;
        }

        // Return the number of files in the activity directory.
        return activityDirectory.list().length;
    }

    /**
     * Exports the activity trial files from the internal application directory to an
     * accessible public directory.
     *
     * @param context Context to access files with
     * @return True if the export was successful.
     */
    public static boolean export(Context context) {
        // Open the destination file.
        File exportDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "DataCollection");
        if (!exportDirectory.exists() && !exportDirectory.mkdir()) {
            Log.e("ActivityController", "Can't create export directory");
            return false;
        }

        // Loop through the user directories.
        for (File file : getUserDirectories()) {

            // Copy the files to the export directory.
            if (!copyFileOrDirectory(file.getAbsolutePath(), exportDirectory.getAbsolutePath())) {
                Log.e("ActivityController", String.format(Locale.getDefault(), "Can't create export file %s", file.getAbsoluteFile()));
                return false;
            }
        }
        return true;
    }

    /**
     * Function to copy a file or directory to the given destination.
     * @param source The file to copy
     * @param destination The destination directory
     * @return True if the copying was successful.
     */
    private static boolean copyFileOrDirectory(String source, String destination) {
        try {
            File src = new File(source);
            File dst = new File(destination, src.getName());

            // Recursively copy directory files.
            if (src.isDirectory()) {
                for (String file : src.list()) {
                    String src1 = new File(src, file).getPath();
                    String dst1 = dst.getPath();
                    copyFileOrDirectory(src1, dst1);
                }
                return true;
            } else {
                return copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Function to create the parent directory structure.
     *
     * @param file The file to make a valid parent structure for
     */
    private static void createParentDirectories(File file) {
        if (file.getName().contains(".")) {
            if (!file.getParentFile().exists()) {
                createParentDirectories(file.getParentFile());
            }
            return;
        }

        while (!file.exists()) {
            createParentDirectories(file.getParentFile());
            file.mkdir();
        }
    }

    /**
     * Function to copy a file to the given destination.
     * @param source The file to copy
     * @param destination The destination directory
     * @return True if the copying was successful.
     */
    private static boolean copyFile(File source, File destination) throws IOException {
        // Ensure the parent directories of the destination exist.
        createParentDirectories(destination);

        // Ensure there is a destination file.
        if (destination.exists() || destination.createNewFile()) {

            // Copy the data.
            try (FileChannel sourceChannel = new FileInputStream(source).getChannel();
                 FileChannel destinationChannel = new FileOutputStream(destination).getChannel()) {
                destinationChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a unique filename for the trial.
     *
     * @param userId The user completing the activity
     * @param activityName The name of the activity
     * @param trialId The trial ID for the user
     * @return A unique filename.
     */
    private static String getTrialFilename(int userId, String activityName, int trialId) {
        return String.format(Locale.getDefault(), "%s_user_%d_trial_%d.txt", activityName, userId, trialId);
    }

    /**
     * Function to start recording the accelerometer data of the Microsoft Band 2.
     *
     * @param context Context to access files with
     * @param listener Listener to notify on success or failure
     * @param userId The id of the participant
     * @param activityName The name of the activity to record
     * @param duration The max duration of the activity
     */
    public static void recordActivity(final Context context, IDataListener<String> listener, int userId, String activityName, int duration) {
        // Work out how many trials the user has already completed for this activity.
        int completedTrials = getCompletedActivityCount(userId, activityName);

        // Open the application files.
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "DataCollection");
        if (!directory.exists() && !directory.mkdir()) {
            listener.onFailure("Can't create application directory in documents.");
            return;
        }

        // Open the user-specific directory.
        directory = new File(directory, String.format(Locale.getDefault(), "user_%d", userId));
        if (!directory.exists() && !directory.mkdir()) {
            listener.onFailure("Can't create user directory");
            return;
        }

        // Open the activity-specific directory.
        directory = new File(directory, activityName);
        if (!directory.exists() && !directory.mkdir()) {
            listener.onFailure("Can't create activity directory");
            return;
        }

        // Create the unique file.
        File trialFile = new File(directory, getTrialFilename(userId, activityName, completedTrials + 1));

        // If the file doesn't exist, create it.
        if (!trialFile.exists()) {
            try {
                if (!trialFile.createNewFile()) {
                    listener.onFailure(String.format(Locale.getDefault(), "Can't create test file %d", completedTrials + 1));
                    return;
                }
            } catch (IOException ex) {
                listener.onFailure(String.format(Locale.getDefault(), "IO creating test file %d", completedTrials + 1));
                return;
            }
        }

        // Get the BandController instance.
        final BandController controller = BandController.getInstance((android.app.Activity) context);

        // Start the trial.
        trialRecorder = new TrialRecorder(controller, trialFile, listener, duration);
    }

    /**
     * Checks if a user is new by seeing if their directory has been created yet.
     *
     * @param userId The user to check
     * @return True if the user hasn't completed any activities.
     */
    public static boolean isNewUser(int userId) {
        File userDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                String.format(Locale.getDefault(), "DataCollection/user_%d", userId));
        return !userDirectory.exists();
    }

    /**
     * Function to delete the last trial for a user's activity.
     *
     * @param userId The user who's trial we want to delete
     * @param activityTitle The title of the activity undergone
     * @return True if deletion was successful.
     */
    public static boolean deleteLastTrial(int userId, String activityTitle) {
        // Iterate through the user directories.
        for (File userDirectory : getUserDirectories()) {

            // Find the directory with the matching user ID.
            if (Integer.valueOf(userDirectory.getName().split("_")[1]) == userId) {

                // Iterate through the user's activities.
                for (File activityDirectory : userDirectory.listFiles()) {

                    // Find the matching activity directory.
                    if (activityDirectory.getName().equals(activityTitle)) {

                        // Delete the most recent trial.
                        int trialId = activityDirectory.list().length;
                        File trialFile = new File(activityDirectory, getTrialFilename(userId, activityTitle, trialId));

                        return trialFile.exists() && trialFile.delete();
                    }
                }
                break;
            }
        }
        return false;
    }

    /**
     *
     * @param activityName The name of the activity.
     * @return True if there are no existing activities with that name.
     */
    public static boolean isNewActivity(String activityName) {
        Realm realm = Realm.getDefaultInstance();
        Activity activity = realm.where(Activity.class).equalTo("title", activityName).findFirst();
        realm.close();

        return activity == null;
    }

    public static void addActivity(String activityName) {
        Realm realm = Realm.getDefaultInstance();
        int id = realm.where(Activity.class).max("id").intValue() + 10;

        Activity activity = new Activity(id, activityName, MAX_TRIALS, DEFAULT_DESCRIPTION);
        realm.beginTransaction();
        realm.copyToRealm(activity);
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Class to handle writing trial data to a file.
     *
     * If the call to 'controller.startListening()' is successful, 'onConnected()' will be
     * triggered which will open the FileOutputStream.
     *
     * Each new data vector is appended to the file until the Band disconnects.
     */
    private static class TrialRecorder implements BandController.IBandEventListener {

        private final BandController        controller;
        private final File                  trialFile;
        private final IDataListener<String> listener;

        private final Handler  stopHandler  = new Handler();
        private final Runnable stopRunnable = new Runnable() {
            @Override
            public void run() {
                controller.stopListening();
            }
        };

        private FileWriter fileWriter;
        private long dataCount = 0;

        private TrialRecorder(BandController controller, File trialFile, IDataListener<String> listener, long duration) {
            this.controller = controller;
            this.trialFile  = trialFile;
            this.listener   = listener;

            // Subscribe to data events.
            controller.subscribe(this);
            controller.startListening();

            // Stop listening to data after the set duration.
            stopHandler.postDelayed(stopRunnable, duration);
        }

        @Override
        public void onConnected() {
            try {
                fileWriter = new FileWriter(trialFile);
            } catch (IOException e) {
                e.printStackTrace();
                listener.onFailure("IOException creating file writer.");
                terminate();
                trialFile.delete();
            }
        }

        @Override
        public void onDisconnected() {
            // Close the output stream.
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.onFailure("IOException closing file writer onDisconnected.");
                }
            }

            if (dataCount > 0) {
                controller.onRecordingComplete(trialFile.getAbsolutePath());
                listener.onSuccess(trialFile.getAbsolutePath());
            } else {
                listener.onFailure("No data to save.");
                trialFile.delete();
            }
            terminate();
        }

        @Override
        public void onError(String error) {
            // Let the listener know there was a problem.
            listener.onFailure(error);
            terminate();
        }

        @Override
        public void onData(String data) {
            if (fileWriter != null) {
                try {
                    fileWriter.append(data);
                    fileWriter.flush();
                    dataCount++;
                } catch (IOException e) {
                    listener.onFailure("IOException writing to file");
                    e.printStackTrace();
                    terminate();
                }
            } else {
                listener.onFailure("File writer null");
                terminate();
            }
        }

        @Override
        public void onTrialComplete(String filename) {
            // Ignore. We trigger this method by calling 'onRecordingComplete'.
        }

        /**
         * Unsubscribe from future events.
         */
        private void terminate() {
            // Stop the timer.
            stopHandler.removeCallbacks(stopRunnable);

            // Stop listening to band events.
            controller.unsubscribe(this);

            // Delete the singleton TrialRecorder instance.
            trialRecorder = null;
        }
    }
}