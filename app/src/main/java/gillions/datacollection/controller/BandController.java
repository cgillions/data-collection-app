package gillions.datacollection.controller;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.sensors.BandAccelerometerEvent;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.SampleRate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Class to handle Microsoft Band 2 functionality.
 */

public class BandController implements BandAccelerometerEventListener {

    // Error flags.
    private static final String ERROR_BAND_NOT_CONNECTED = "Please connect to the Microsoft Band.";

    // Variables.
    private List<IBandEventListener> listeners;
    private Activity                 activity;
    private BandClient               client    = null;
    private boolean                  listening = false;

    // Singleton instance.
    private static BandController sInstance;
    private BandController(Activity activity) {
        this.activity = activity;
    }

    /**
     * An activity is needed in order to push events on the UI thread.
     *
     * @param activity A valid, live Activity
     * @return The BandController instance.
     */
    public static BandController getInstance(Activity activity) {
        if (sInstance == null) {
            // Create the controller.
            sInstance = new BandController(activity);
        } else {
            // Update the context.
            sInstance.activity = activity;
        }
        return sInstance;
    }

    /**
     * Subscribe to Band events.
     *
     * @param eventListener The listener to subscribe
     */
    public void subscribe(IBandEventListener eventListener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(eventListener);
    }

    /**
     * Unsubscribe from Band events.
     *
     * @param eventListener The listener to unsubscribe
     */
    public void unsubscribe(IBandEventListener eventListener) {
        if (listeners != null) {
            listeners.remove(eventListener);

            // Destroy instance if nobody is listening.
            if (listeners.size() == 0) {
                disconnect(false);
                sInstance = null;
            }
        }
    }

    /**
     * Function to start receiving accelerometer data from the Band.
     */
    public void startListening() {
        if (!listening) {
            new RecordAccelerometerTask(this).execute();
            listening = true;
        }

    }

    /**
     * Function to stop receiving accelerometer data from the Band.
     */
    public void stopListening() {
        disconnect(true);
    }

    /**
     * Call this function when a trial for an activity finishes.
     *
     * @param filename The name of the trial file
     */
    public void onRecordingComplete(String filename) {
        if (listeners == null) {
            // Something's gone wrong. Delete the erroneous file.
            File file = new File(filename);
            if (file.exists()) {
                file.delete();
            }
            return;
        }

        for (IBandEventListener listener : listeners) {
            listener.onTrialComplete(filename);
        }
    }

    /**
     * Function to disconnect from the Band.
     *
     * @param notify True if listeners should be notified.
     */
    private void disconnect(boolean notify) {
        // Disconnect from the Band.
        if (client != null) {
            try {
                Log.e("BandController", "Disconnecting");
                client.getSensorManager().unregisterAllListeners();
                client.disconnect().await();
                Log.e("BandController", "Disconnected");
            } catch (InterruptedException | BandException | IllegalArgumentException e) {
                e.printStackTrace();
            }
            client = null;
        }
        listening = false;

        // Notify the listeners on the UI thread.
        if (notify && listeners != null && activity != null) {
            Log.e("BandController", "Notifying listeners about disconnect");
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (IBandEventListener listener : listeners) {
                        listener.onDisconnected();
                    }
                }
            });
        }
    }

    @Override
    public void onBandAccelerometerChanged(BandAccelerometerEvent event) {
        onData(event.getTimestamp(), event.getAccelerationX(), event.getAccelerationY(), event.getAccelerationZ());
    }

    /**
     * Function to push the error to any listeners.
     *
     * @param error The error message
     */
    private void onError(final String error) {
        // Sanity check.
        if (listeners == null) {

            // If no-one is listening, ensure we are disconnected from the Band.
            if (client != null) {
                client.disconnect();
                client = null;
            }
            return;
        }

        // Push the message on the UI thread.
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (IBandEventListener listener : listeners) {
                    listener.onError(error);
                }
            }
        });
    }

    /**
     * Function to push accelerometer data to any listeners.
     *
     * @param x X acceleration
     * @param y Y acceleration
     * @param z Z acceleration
     */
    private void onData(long timestamp, float x, float y, float z) {
        // If no-one is listening, ensure we are disconnected from the Band.
        if (listeners == null) {
            disconnect(false);
            return;
        }

        // Format the data.
        final String data = String.format(Locale.getDefault(), "%d,%f,%f,%f\n", timestamp, x, y, z);

        // Push the data.
        for (IBandEventListener listener : listeners) {
            listener.onData(data);
        }
    }

    /**
     * Function to connect to the Band.
     *
     * @return True if connected
     * @throws InterruptedException When attempting to make a connection.
     * @throws BandException When attempting to make a connection.
     */
    private boolean connectToBand() throws InterruptedException, BandException {


        if (client == null) {
            // Get the paired devices.
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();

            // Check if the band is paired to the device.
            if (devices.length == 0) {
                onError(ERROR_BAND_NOT_CONNECTED);
                return false;
            }

            // Connect to the band.
            client = BandClientManager.getInstance().create(activity, devices[0]);
            return client.connect().await() == ConnectionState.CONNECTED;
        }

        // Already have a client object. Test if we are connected.
        else return client.getConnectionState() == ConnectionState.CONNECTED
                    || client.connect().await() == ConnectionState.CONNECTED;
    }

    /**
     * AsyncTask to handle connecting to the Band in the background.
     */
    private static class RecordAccelerometerTask extends AsyncTask<Void, Void, Void> {

        // Hold the instance of the BandController.
        private final BandController controller;
        private RecordAccelerometerTask(BandController controller) {
            this.controller = controller;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // Try to connect to the band.
                Log.e("BandController", "Connecting");
                if (controller.connectToBand()) {
                    Log.e("BandController", "Connected");

                    // Post the event to the listeners if successful.
                    controller.activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (IBandEventListener listener : controller.listeners) {
                                listener.onConnected();
                            }
                        }
                    });

                    // Subscribe to accelerometer events.
                    controller.client.getSensorManager().registerAccelerometerEventListener(
                            controller, SampleRate.MS32);
                } else {
                    // Ensure all connections are ceased.
                    Log.e("BandController", "Not connected");
                    controller.disconnect(true);
                }
            } catch (InterruptedException | BandException ex) {
                controller.onError(ERROR_BAND_NOT_CONNECTED);
                ex.printStackTrace();
            }

            return null;
        }
    }

    /**
     * Created by Charlie on 07/02/2018.
     *
     * Interface for classes that wish to be notified of Band events.
     */
    public interface IBandEventListener {

        /**
         * Called when the Microsoft Band 2 connects.
         */
        void onConnected();

        /**
         * Called when the Microsoft Band 2 disconnects.
         */
        void onDisconnected();

        /**
         * Called when an error occurs on the Microsoft Band 2.
         *
         * @param error The error message
         */
        void onError(String error);

        /**
         * Called when new data is pushed from the Microsoft Band 2.
         *
         * @param data Accelerometer data formatted as 'x,y,z\n'
         */
        void onData(String data);

        /**
         * Called when an activity trial is completed.
         *
         * @param filename The path to the data file
         */
        void onTrialComplete(String filename);
    }
}