package gillions.datacollection.controller;

import android.content.Context;

import gillions.datacollection.mvp.model.Activity;
import gillions.datacollection.mvp.model.Preferences;
import io.realm.Realm;

/**
 * Created by Charlie on 07/02/2018.
 *
 * Class to handle Realm-related functionality.
 */

public class RealmController {

    /**
     * Function to initialise the Ream database.
     *
     * @param context Context to initialise Realm with
     */
    public static void initDatabase(Context context) {
        // Initialise realm.
        Realm.init(context);

        // Check if there is no data in the database.
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(Activity.class).findAll().size() == 0) {

            // Add the activity data.
            realm.beginTransaction();
            realm.copyToRealm(ActivityController.getActivities());
            realm.commitTransaction();

            // Newest activities will be added, so set the database version.
            Preferences.setDatabaseVersion(context, 1);
            return;
        }
        // If there is, check what version of the DB we are at.
        long version = Preferences.getDatabaseVersion(context);

        // New activities were added at version 1.
        if (version == 0) {

            // Update the activities.
            realm.beginTransaction();
            realm.delete(Activity.class);
            realm.copyToRealm(ActivityController.getActivities());
            realm.commitTransaction();

            // Set the database version.
            Preferences.setDatabaseVersion(context, 1);
        }
    }
}